# Data file

Date file contains: 
- python script which was used to run measurements
- .dat file with extracted VNA data
- meta file, explaining globle used settings from the measurement
- github id text file

Read name of file:
- Zoom_STARK_1   =  zoomed in power sweep of Stark tone at fixed frequency, high resolution
- date
- Fix3  = internally used name of measured qubit on HBAR-transmon device
- spec  = power sweep spectroscopy