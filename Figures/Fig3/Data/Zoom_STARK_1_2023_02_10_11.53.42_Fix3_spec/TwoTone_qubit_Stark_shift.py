import stlab, time
from stlab.devices.PNAN5222A import PNAN5222A
from stlab.devices import autodetect_instrument #Import device driver for your desired PNA
import numpy as np
import stlab.advanced_measurements.TwoToneSpectroscopy as tts
from stlab.devices.Keysight_B2901A import Keysight_B2901A #Import device driver for your desired PNA
from stlab.devices.RS_SMB100A import RS_SMB100A

prefix = 'Zoom_STARK_' #prefix for measurement folder name.  Can be anything or empty
idstring = 'Fix3_spec_Reso'  #Additional info included in measurement folder name.  Can be anything or empty
idstring2 = 'Fix3_spec'

pna     = PNAN5222A(addr='TCPIP::192.168.1.221::hislip0::INSTR',reset=True,verb=True) #Initialize device communication and reset
rss = RS_SMB100A(addr='TCPIP::192.168.1.25::INSTR', reset=True)
################################################################ 

#Transmon-Res
res_f0=7.4162e9         
res_delta=20e6
res_points=201
res_ifbw=10
res_pow=-10
#estimate 8-10hr
q_start=6.00e9
q_stop=6.25e9
q_points=2001
q_ifbw=5
q_power = 2

# RS generator parameters
Stark_Detune = 30e6
freq_Q = 6.09072e9
rs_gen_freq =  freq_Q + Stark_Detune
# rs_gen_freq   = 6.12072e9

STARKstart = -12    
STARKstop = 2
STARK_steps = int((STARKstop-STARKstart)*10)+1
STARKlist=np.linspace(STARKstart,STARKstop,STARK_steps)

#turn on RS generator
rss.EXTref()
rss.SetFrequency(rs_gen_freq)
rss.SetPower(STARKlist[0])
rss.RFon()

#list of attenuators:
attn_p=-60          #attenuation probe line
attn_d=-40          #attenuation drive line
att_in=-48.0        #attenuation inside fridge, cables not included
ampl_out = 1.0      #number of RT amplifiers

for i,STARK in enumerate(STARKlist):
    # rss.SetFrequency(rs_gen_freq)
    rss.SetPower(STARK)

    tts.TwoToneSetup(pna,
    f_probe_start = res_f0-res_delta/2, 
    f_probe_stop = res_f0+res_delta/2, 
    f_probe_points = res_points,
    probe_ifbw = res_ifbw,
    probe_power = res_pow,
    f_pump_start =  q_start,
    f_pump_stop = q_stop,  
    f_pump_points = q_points,   
    pump_ifbw = q_ifbw, 
    pump_power = q_power,
    searchtype='MIN')

    #pna.write('SENS:AVER OFF')
    pna.write('SENS:AVER:COUN 1')
    pna.write('SENS:AVER ON')
    pna.write('SENS:AVER:CLEAR')
    naver = int(pna.query('SENS:AVER:COUN?'))

    for ii in range(naver-1):
            pna.query("INIT:IMM;*OPC?")
    f_probe = tts.TwoToneProbeSet(pna,searchtype='MIN')
    # f_probe = res_f0
    tts.TwoToneSetProbeFrequency(pna,f_probe)

    pna.write('SENS2:AVER:COUN 2')
    pna.write('SENS2:AVER ON')
    pna.write('SENS2:AVER:CLEAR')
    naver = int(pna.query('SENS2:AVER:COUN?')); print(naver)

    for ii in range(naver-1):
        pna.query("INIT2:IMM;*OPC?")
    result = tts.TwoToneMeasure(pna)

    result[0]['Probe Power (dBm)'] = res_pow
    result[0]['Drive Power (dBm)'] = q_power
    result[0]['VNA Port 1 RT Attn (dB)']=attn_p
    result[0]['VNA Port 2 RT Attn (dB)']=attn_d
    result[0]['Line inside fridge Attn (dB)']=att_in
    result[0]['Number Miteqs']=ampl_out 
    result[0]['Stark freq [Hz]'] = rs_gen_freq
    result[0]['Stark power [dBm]'] = STARK

    result[1]['Probe Power (dBm)'] = res_pow
    result[1]['Drive Power (dBm)'] = q_power
    result[1]['VNA Port 1 RT Attn (dB)']=attn_p
    result[1]['VNA Port 2 RT Attn (dB)']=attn_d
    result[1]['Line inside fridge Attn (dB)']=att_in
    result[1]['Number Miteqs']=ampl_out
    result[1]['Stark freq [Hz]'] = rs_gen_freq
    result[1]['Stark power [dBm]'] = STARK
    
    if i==0: #if on first measurement, create new measurement file and folder using titles extracted from measurement
        myfile = stlab.newfile(prefix,idstring,result[0].keys(), autoindex=True)
        myfile2 = stlab.newfile(prefix,idstring2,result[1].keys(), autoindex=True)
    
    stlab.saveframe(myfile,result[0])
    stlab.metagen.fromarrays(myfile,result[0]['Frequency (Hz)'],STARKlist[0:i+1],xtitle='Frequency (Hz)',ytitle='Stark power [dBm]',colnames='auto')

    stlab.savedict(myfile2, result[1]) #Save measured data to file.  Written as a block for spyview.
    stlab.metagen.fromarrays(myfile2,result[1]['Frequency (Hz)'],STARKlist[0:i+1],xtitle='Frequency (Hz)',ytitle='Stark power [dBm]',colnames='auto')

#Stark RF source
rss.RFoff()
#VNA and files
tts.TwoToneSourcesOff(pna)
myfile.close()
myfile2.close()
pna.close() #close pna
#mw.RFoff()