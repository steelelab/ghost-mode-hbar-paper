#imports
import stlab, time, math
from stlab.devices.PNAN5222A import PNAN5222A
from stlab.devices import autodetect_instrument #Import device driver for your desired PNA
import numpy as np
import stlab.advanced_measurements.TwoToneSpectroscopy as tts
from stlab.devices.Keysight_B2901A import Keysight_B2901A #Import device driver for your desired PNA
from stlab.devices.RS_SMB100A import RS_SMB100A

#Set file names
prefix = 'Hbar_TT'                               #prefix for measurement folder name.  Can be anything or empty
idstring = 'segm_Qubit_RO'                #Additional info included in measurement folder name.  Can be anything or empty
idstring2 = 'segm_Qubit'

## EQUIPMENT:
pna      = PNAN5222A(addr='TCPIP::192.168.1.221::hislip0::INSTR',reset=True,verb=True) #Initialize device communication and reset
rss      = RS_SMB100A(addr='TCPIP::192.168.1.25::INSTR', reset=True)

#SETTINGS:
## RO:
res_f0 = 7.0654e9;   res_delta = 35e6
# res_start = 7.0585e9;  res_stop = 7.0757e9
res_points = 1001
res_ifbw = 50
RO_pow =-20
RO_aver = 5

#Qubit Settings
#full range:
q_start=5.900e9   
q_stop=6.100e9 
q_steps = 2001
Q_freq_list=np.linspace(q_start,q_stop,q_steps)
q_ifbw = 5
#Number of points between coarse segment
Coarse_freq_diff = 250e3
#HBAR modes frequencies, points, fine segm point number:
Hbar_freq = [5.96193e9,6.05907e9]   
Fine_freq_diff = 0.25e3
df_hbar = 200e3

powstart = -20   
powstop = 14
pow_steps = int((powstop-powstart)*1)+1
Q_powers=np.linspace(powstart,powstop,pow_steps)
# Q_powers=[-4, -2, 0, 2, 4, 6, 8, 10]     
q_aver = 8

#other fridge numbers and settings:
attn_p=-60          #attenuation probe line
attn_d=-40          #attenuation drive line
att_in=-48.0        #attenuation inside fridge, cables not included
ampl_out = 1.0      #number of RT amplifiers

#Stark Tone:
Stark_Detune = 30e6
freq_Q = 5.9805e9
rs_gen_freq =  freq_Q + Stark_Detune
Stark_pow = -10.5 #dBm

#turn on RS generator
rss.EXTref()
rss.SetFrequency(rs_gen_freq)
rss.SetPower(Stark_pow)
rss.RFon()

########################################################################################################################
################ @ this point in Normal measurement script you start tts.TwoToneSetup(pna, ....) #######################
########################################################################################################################
for j,Q_pow in enumerate(Q_powers):
    #### SETUP TWO-TONE MEASUREMENT:
    searchtype = 'MIN'
    #clear and setup traces
    pna.write('*RST')      #stop any OPCs and go to preset (same as 'Preset' button) (overkill) (better)
    pna.write('SYST:FPR')  #do reset with all windows and traces deleted
    #setup two displays
    pna.write("DISP:WIND1:STATE ON")
    pna.write("DISP:WIND2:STATE ON")

    #setup RO:
    ###setup probe scan (magnitude)
    pna.write("CALC1:PAR:DEF:EXT 'CH1_S21_S1', 'B,1'")
    pna.write("DISP:WIND1:TRACE1:FEED 'CH1_S21_S1'")
    pna.write("CALC1:PAR:SEL 'CH1_S21_S1'")
    pna.write("CALC1:FORM MLOG")
    #setup triggering per channel
    pna.write("INIT:CONT OFF")
    pna.write("SENS1:SWE:MODE HOLD")
    pna.write("TRIG:SCOP CURR")
    ###setup probe scan more
    pna.write("SENS:FREQ:START %s" % (res_f0-res_delta/2))
    pna.write("SENS:FREQ:STOP %s" % (res_f0+res_delta/2))
    pna.write("SENS:SWE:POIN %s" % (res_points))
    pna.write("SENS:BWID %s" % (res_ifbw))
    pna.write("SOUR:POW1 %s" %(RO_pow))
    #do settings for Marker for the probe
    pna.write("CALC1:MARK:REF ON")
    pna.write("CALC1:MARK:REF:X " + str(res_f0-res_delta/2))
    pna.write("CALC1:MARK1 ON")
    pna.write("CALC1:MARK1:FUNC {}".format(searchtype))
    pna.write("CALC1:MARK1:FUNC:TRAC ON")
    pna.write("CALC1:MARK1:DELT ON")
    pna.write("CALC1:MARK2 ON")
    pna.write("CALC1:MARK2:FUNC {}".format(searchtype))
    pna.write("CALC1:MARK2:FUNC:TRAC ON")

    #setup qubit:
    ###setup two tone scan
    pna.write("CALC2:PAR:DEF:EXT 'CH2_S21_S1', 'B,1'")
    pna.write("DISP:WIND2:TRACE1:FEED 'CH2_S21_S1'")
    pna.write("CALC2:PAR:SEL 'CH2_S21_S1'")
    pna.write("CALC2:FORM MLOG")
    ##
    pna.write("SENS2:FREQ:START %s" % (q_start))
    pna.write("SENS2:FREQ:STOP %s" % (q_stop))
    pna.write("SENS2:SWE:POIN %s" % (q_steps))
    pna.write("SENS2:BWID %s" % (q_ifbw))
    ########################################################################################################################
    #SEGMENTATION QUBIT:
    ##Delete all segments:
    pna.write('SENS2:SEGM:DEL:ALL')
    ##Turn ON segment sweep:
    pna.write('SENS2:SEGM:ARB ON')
    ##Make first coarse segment (add, frequency range, points, IFBW):
    pna.write('SENS2:SEGM1:ADD')
    pna.write('SENS2:SEGM1:FREQ:STAR {}'.format(q_start))
    pna.write('SENS2:SEGM1:FREQ:STOP {}'.format((Hbar_freq[0]-df_hbar/2)))
    pna.write('SENS2:SEGM1:SWE:POIN {}'.format(int(math.ceil(((Hbar_freq[0]-df_hbar/2)-q_start)/Coarse_freq_diff))))
    pna.write('SENS2:SEGM1 ON')
    ###Loop segments:
    k=0
    for i, h_freq in enumerate(Hbar_freq):
        #fine segments:
        pna.write('SENS2:SEGM{}:ADD'.format(k+i+2))
        pna.write('SENS2:SEGM{}:FREQ:STAR {}'.format((k+i+2), h_freq-df_hbar/2))
        pna.write('SENS2:SEGM{}:FREQ:STOP {}'.format((k+i+2), h_freq+df_hbar/2))
        pna.write('SENS2:SEGM{}:SWE:POIN {}'.format((k+i+2), int(math.ceil(((h_freq+df_hbar/2) - (h_freq-df_hbar/2))/Fine_freq_diff))))
        pna.write('SENS2:SEGM{} ON'.format(k+i+2))
        #coarse segments:
        if i!=len(Hbar_freq)-1:
             #coarse segments between 2 fine Hbar segments:
            pna.write('SENS2:SEGM{}:ADD'.format(k+i+3))
            pna.write('SENS2:SEGM{}:FREQ:STAR {}'.format((k+i+3), h_freq+df_hbar/2))
            pna.write('SENS2:SEGM{}:FREQ:STOP {}'.format((k+i+3), Hbar_freq[i+1]-df_hbar/2))
            pna.write('SENS2:SEGM{}:SWE:POIN {}'.format((k+i+3), int(math.ceil(((Hbar_freq[i+1]-df_hbar/2)-(h_freq+df_hbar/2))/Coarse_freq_diff))))
            pna.write('SENS2:SEGM{} ON'.format(k+i+3))
        else:
            print('loop done')
        #adding k
        k += 1  
    #Final coarse segment:
    pna.write('SENS2:SEGM{}:ADD'.format(2*len(Hbar_freq)+1))
    pna.write('SENS2:SEGM{}:FREQ:STAR {}'.format((2*len(Hbar_freq)+1), Hbar_freq[-1]+df_hbar/2))
    pna.write('SENS2:SEGM{}:FREQ:STOP {}'.format((2*len(Hbar_freq)+1), q_stop))
    pna.write('SENS2:SEGM{}:SWE:POIN {}'.format((2*len(Hbar_freq)+1), int(math.ceil(((q_stop)-(Hbar_freq[-1]+df_hbar/2))/Coarse_freq_diff))))
    pna.write('SENS2:SEGM{} ON'.format(2*len(Hbar_freq)+1))
    ##set sweep type:
    pna.write('SENS2:SWE:TYPE SEGM')
    ########################################################################################################################
    ##
    pna.write("SENS2:FOM:STATE 1")      #switch on Frequency Offset Module
    pna.write("SENS2:FOM:RANG3:COUP 0")     #decouple Receivers
    pna.write("SENS2:FOM:RANG2:COUP 0")     #decouple Source
    ##
    pna.write("SENS2:FOM:RANG3:SWE:TYPE CW")    #set Receivers in CW mode
    pna.write("SENS2:FOM:RANG2:SWE:TYPE CW")    #set Source in CW mode
    ##
    pna.write("SENS2:FOM:RANG3:FREQ:CW %s" %(res_f0-res_delta/2)) #set cw freq to receivers
    pna.write("SENS2:FOM:RANG2:FREQ:CW %s" %(res_f0-res_delta/2)) #set cw freq to source1
    ##
    pna.write("SENS2:FOM:DISP:SEL 'Primary'")       #set x-axis to primary
    ##
    pna.write("SOUR2:POW:COUP 0")                   #decouple powers
    pna.write("SOUR2:POW1 %s" %(RO_pow))
    pna.write("SOUR2:POW3 %s" %(Q_pow))
    pna.write("SOUR2:POW3:MODE ON")                 #switch on port3

    pna.write("CALC2:MARK1 ON")
    if searchtype == 'MAX':
        pna.write("CALC2:MARK1:FUNC MIN")
    elif searchtype == 'MIN':
        pna.write("CALC2:MARK1:FUNC MAX")
    else:
        raise ValueError('Bad search type')
    pna.write("CALC2:MARK1:FUNC:TRAC ON")
    ########################################################################################################################
    ###################################### Continue Normal measurement script ##############################################
    ########################################################################################################################
    #pna.write('SENS:AVER OFF')
    pna.write('SENS:AVER:COUN {}'.format(RO_aver))
    pna.write('SENS:AVER ON')
    pna.write('SENS:AVER:CLEAR')
    naver = int(pna.query('SENS:AVER:COUN?'))

    for ii in range(naver-1):
            pna.query("INIT:IMM;*OPC?")
    f_probe = tts.TwoToneProbeSet(pna,searchtype='MIN')
    # f_probe = res_f0
    tts.TwoToneSetProbeFrequency(pna,f_probe)

    pna.write('SENS2:AVER:COUN {}'.format(q_aver))
    pna.write('SENS2:AVER ON')
    pna.write('SENS2:AVER:CLEAR')
    naver = int(pna.query('SENS2:AVER:COUN?')); print(naver)
    pna.write('SENS2:TRIG:CHAN:AUX1')

    for ii in range(naver-1):
        pna.query("INIT2:IMM;*OPC?")
    result = tts.TwoToneMeasure(pna)

    ########################################################################################################################
    ###################### In Normal measurement script the saving should start here #######################################
    ########################################################################################################################
    result[0]['Probe Power (dBm)'] = RO_pow
    result[0]['Drive Power (dBm)'] = Q_pow
    result[0]['VNA Port 1 RT Attn (dB)']=attn_p
    result[0]['VNA Port 2 RT Attn (dB)']=attn_d
    result[0]['Line inside fridge Attn (dB)']=att_in
    result[0]['Number Miteqs']=ampl_out 
    # result[0]['Stark freq [Hz]'] = rs_gen_freq
    # result[0]['Stark power [dBm]'] = STARK

    result[1]['Probe Power (dBm)'] = RO_pow
    result[1]['Drive Power (dBm)'] = Q_pow
    result[1]['VNA Port 1 RT Attn (dB)']=attn_p
    result[1]['VNA Port 2 RT Attn (dB)']=attn_d
    result[1]['Line inside fridge Attn (dB)']=att_in
    result[1]['Number Miteqs']=ampl_out
    # result[1]['Stark freq [Hz]'] = rs_gen_freq
    # result[1]['Stark power [dBm]'] = STARK

    if j==0: #if on first measurement, create new measurement file and folder using titles extracted from measurement
        myfile = stlab.newfile(prefix,idstring,result[0].keys(), autoindex=True)
        myfile2 = stlab.newfile(prefix,idstring2,result[1].keys(), autoindex=True)

    stlab.saveframe(myfile,result[0])
    stlab.metagen.fromarrays(myfile,result[0]['Frequency (Hz)'],Q_powers[0:j+1],xtitle='Frequency (Hz)',ytitle='Drive power [dBm]',colnames='auto')

    stlab.savedict(myfile2, result[1]) #Save measured data to file.  Written as a block for spyview.
    stlab.metagen.fromarrays(myfile2,result[1]['Frequency (Hz)'],Q_powers[0:j+1],xtitle='Frequency (Hz)',ytitle='Drive power [dBm]',colnames='auto')
#END OF LOOP

# #Stark RF source
rss.RFoff()
#VNA and files
tts.TwoToneSourcesOff(pna)
myfile.close()
myfile2.close()
pna.close() #close pna
#mw.RFoff()