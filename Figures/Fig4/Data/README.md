# Data file

Date file contains: 
- python script which was used to run measurements
- .dat file with extracted VNA data
- meta file, explaining globle used settings from the measurement
- github id text file

Read name of file:
- Hbar_TT1   =  HBAR device performed Two-Tone spectroscopy
- date
- segm_Qubit  = segmented VNA spectroscopy where qubit datapoint spacing is different from around HBAR resonance datapoint spacing