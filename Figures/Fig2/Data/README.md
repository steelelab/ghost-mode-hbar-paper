# Data file

Date file contains: 
- python script which was used to run measurements
- .dat file with extracted VNA data
- meta file, explaining globle used settings from the measurement
- github id text file

Read name of file:
- Stark_Qdrive_BLUE_Pswp1   =  power sweep of blue detuned Stark tone
- date
- Fix3  = internally used name of measured qubit on HBAR-transmon device
- pw_spec  = power sweep spectroscopy